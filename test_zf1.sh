#Petit script pour tester le curl pour pusher sur Prometheus

#zf190129.1403

#prometheus_ip="172.22.0.1"
prometheus_ip="127.0.0.1"

label=toto
t=5
c=2


    cat <<__EOF | curl --data-binary @- http://$prometheus_ip:9091/metrics/job/zuzuresenti/instance/$label
        # TYPE zuzu_resenti_load_time gauge
        zuzu_resenti_load_time {location="berlin"} $t
        # TYPE zuzu_resenti_page_changed gauge
        zuzu_resenti_page_changed $c
__EOF


